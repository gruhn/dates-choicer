class EventsController < ApplicationController
  before_filter :set_event, only: [:show, :vote, :vote_creat, :vote_edit, :vote_update, :event_result_creat, :edit, :update, :destroy]

  def index
    authorize Event
    if pundit_user
      @events = pundit_user.events
    end
  end

  def show
    authorize @event
    @users = Event.find(params[:id]).users

    voted =  @event.votes

    @percents, @votes = statistics(voted)

    if @event.deadline_date < Date.today
      result = event_result(voted)
      if result.length == 1
        @event.create_event_result(date: result[0])
      end
      if result.length > 1
        @event_result = result
      end

      @show_event_result = @event.event_result
    end
  end

  def vote
    authorize @event

    @dates = @event.iniatial_date..@event.final_date

    @votes = Array.new(@dates.count, Vote.new)
  end

  def vote_creat
    params[:votes].each do |vote|
      new_vote = current_user.votes.new(vote_params(vote))
      new_vote.event_id = @event.id
      new_vote.save
    end
    redirect_to @event
  end

  def vote_edit
    authorize @event

    @votes = Vote.where(user_id: pundit_user.id, event_id: @event.id)
  end

  def vote_update
    params[:votes].each do |key, vote|
      pundit_user.votes.find(key).update(vote_params(vote))
    end
    redirect_to @event
  end

  def new
    @event = Event.new
    authorize @event
  end

  def edit
    authorize @event
  end

  def event_result_creat
    if params[:result_vote].present?
      @event.create_event_result(date: params[:result_vote])
    end
    redirect_to @event
  end

  def create
    @event = pundit_user.owned_events.new(event_params)

    authorize @event

    if @event.save
      redirect_to @event
    else
      render 'new'
    end
  end

  def update
    authorize @event

    if @event.update(event_params)
      redirect_to @event
    else
      render 'edit'
    end
  end

  def destroy
    authorize @event
    @event.destroy

    redirect_to events_path
  end

  private

  def event_result(voted)
    votes_per_dates = voted.inject(Hash.new(0)) { |h, v| h[v.date] += v.possible_answer; h }
    max_votes = votes_per_dates.values.max

    result = []
    votes_per_dates.each do |date, vote|
      if vote == max_votes
        result << date
      end
    end
    return result
  end

  def statistics(voted)
    votes_per_dates = voted.inject(Hash.new(0)) { |h, v| h[v.date] += v.possible_answer; h }
    max_votes = votes_per_dates.values.max

    percents_per_dates = votes_per_dates.inject(Hash.new) {|h,v| h[v.first] = v.last*100/max_votes; h}

    return percents_per_dates, votes_per_dates
  end

  def vote_params(vote)
    vote.permit(:date, :possible_answer)
  end

  def event_params
    event = params.require(:event).permit(:title, :comment, :deadline_date, :final_date, :iniatial_date, user_ids: [])
    event["user_ids"] << pundit_user.id
    return event
  end

  def set_event
    @event = Event.find(params[:id])
  end
end
