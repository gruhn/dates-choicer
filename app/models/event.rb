class Event < ActiveRecord::Base
  belongs_to :owner, class_name:"User"
  has_one :event_result, dependent: :destroy
  has_and_belongs_to_many :users
  has_many :votes, dependent: :destroy
  accepts_nested_attributes_for :users, allow_destroy: true

  validates :title, length:{minimum: 5}
  validates :iniatial_date, :final_date, :deadline_date, presence:true
  # validate :validate_dates
  # validate :valid_iniatial_date
  # validate :valid_final_date

  def validate_dates
    if deadline_date && deadline_date <= Date.today
      errors.add(:deadline_date, I18n.t(:validate_dates))
    end

    if final_date && final_date <= Date.today
      errors.add(:final_date,  I18n.t(:validate_dates))
    end

    if iniatial_date && iniatial_date <= Date.today
      errors.add(:iniatial_date, I18n.t(:validate_dates))
    end
  end

  def valid_iniatial_date
    if iniatial_date && final_date
      if iniatial_date > final_date
        errors.add(:iniatial_date, I18n.t(:valid_iniatial_date))
      end
    end
  end

  def valid_final_date
    if iniatial_date && deadline_date
      if iniatial_date < deadline_date
        errors.add(:deadline_date, I18n.t(:valid_final_date))
      end
    end
  end
end
