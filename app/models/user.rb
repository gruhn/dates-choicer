class User < ActiveRecord::Base

  validates :login, length:{minimum: 3}, uniqueness: true

  has_many :owned_events, class_name: "Event", foreign_key: "owner_id", dependent: :destroy
  has_many :votes, dependent: :destroy
  has_and_belongs_to_many :events
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
