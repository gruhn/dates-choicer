class Vote < ActiveRecord::Base
  belongs_to :event
  belongs_to :user, autosave:true
end
