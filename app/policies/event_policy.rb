class EventPolicy < ApplicationPolicy
  attr_reader :user, :event

  def initialize(user, event)
    @user = user
    @event = event
  end

  def admin?
    user && @event.owner_id == user.id
  end

  def index?
    user
  end

  def show?
    user && @event.users.exists?(user.id)
  end

  def vote?
    user && (@event.users.exists?(user.id) && !Vote.where(event_id: @event.id, user_id: user.id).exists? && @event.deadline_date >= Date.today)
  end

  def vote_edit?
    user && (@event.users.exists?(user.id) && Vote.where(event_id: @event.id, user_id: user.id).exists? && @event.deadline_date >= Date.today)
  end

  def new?
    user
  end

  def edit?
    user && (user.id == @event.owner_id && @event.deadline_date >= Date.today)
  end

  def create?
    user && user.id?
  end

  def update?
    user && user.id == @event.owner_id
  end

  def destroy?
    user && user.id == @event.owner_id
  end

end
