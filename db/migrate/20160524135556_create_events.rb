class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :comment
      t.integer :owner_id
      t.date :deadline_date
      t.date :final_date
      t.date :iniatial_date

      t.timestamps null: false
    end
  end
end
