class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :event_id
      t.integer :user_id
      t.date :date
      t.float :possible_answer, default: 0

      t.timestamps null: false
    end
  end
end
