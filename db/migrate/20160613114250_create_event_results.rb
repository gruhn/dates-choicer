class CreateEventResults < ActiveRecord::Migration
  def change
    create_table :event_results do |t|
      t.date :date
      t.integer :event_id

      t.timestamps null: false
    end
  end
end
